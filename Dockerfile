FROM archlinux

RUN <<EOF
set -e

unifont_ver="16.0.02"
unifont_sha256="521f2b92e8b6bd4ea190cea049a039dde359a6e2cae9458e45d696008fa6997f"

pacman -Syu --noconfirm
pacman -S --noconfirm base-devel freetype2 fuse3 git python ttf-dejavu gptfdisk dosfstools squashfs-tools nano bash-completion less

curl https://ftp.gnu.org/gnu/unifont/unifont-${unifont_ver}/unifont-${unifont_ver}.bdf.gz -o unifont.bdf.gz

if ! echo ${unifont_sha256} unifont.bdf.gz | sha256sum --check - ; then
	exit 1
else
	mkdir -p /usr/share/fonts/unifont
	gzip -d unifont.bdf.gz
	mv unifont.bdf /usr/share/fonts/unifont/unifont.bdf
fi

EOF